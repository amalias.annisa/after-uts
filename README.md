# Story 7 - 10

[![pipeline status](https://gitlab.com/amalias.annisa/after-uts/badges/master/pipeline.svg)](https://gitlab.com/amalias.annisa/after-uts/-/commits/master)


This repository contains of my PPW assignments from story 7 to 10.
- Name  : Annisa Amalia Sholeka
- NPM   : 1906354002


## Link

https://amalia-annisa.herokuapp.com

## License

This template was cloned from [here](https://github.com/laymonage/django-template-heroku).