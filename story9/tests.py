from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from .views import logIn, signUp, logOut
from .apps import Story9Config

# Create your tests here.
class UnitTest(TestCase):
    def test_url_login(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_signup(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_logout(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)
    
    def test_login_page(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')
    
    def test_signup_page(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'signup.html')
    
    def test_check_function_used_by_login(self):
        found = resolve('/login/')
        self.assertEqual(found.func, logIn)

    def test_check_function_used_by_signup(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signUp)
    
    def test_check_login_page_have_form(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)
    
    def test_signin_header(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertContains(response, "Sign in")
        self.assertContains(response, "Don't have an account?")
        self.assertContains(response, "Username:")
        self.assertContains(response, "Password:")

    def test_signup_header(self):
        response = Client().get('/signup/')
        self.assertContains(response, "Create a new account")
        self.assertContains(response, "Have an account already?")
        self.assertContains(response, "Username:")
        self.assertContains(response, "Confirm password:")
        self.assertContains(response, "First name:")
        self.assertContains(response, "Last name:")

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(Story9Config.name, "story9")