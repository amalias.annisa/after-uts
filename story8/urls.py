from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.url_fuction, name="url_function"),
    path('data/', views.fungsi_data, name="fungsi_data"),
]