from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from .views import url_fuction, fungsi_data
from django.apps import apps

from .apps import Story8Config

# Create your tests here.
class UnitTest(TestCase):
    def test_url(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_html(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'search.html')
    
    def test_search_book(self):
        response = Client().get('/story8/data/?q=blender')
        self.assertEqual(response.status_code, 200)
    
    def test_text_story7(self):
        response = Client().get("/story8/")
        html_response = response.content.decode('utf8')
        self.assertIn("Cover", html_response)
        self.assertIn("Title", html_response)
        self.assertIn("Author", html_response)
        self.assertIn("Publisher", html_response)
        self.assertIn("Published Date", html_response)

class TestApp(TestCase):
        def test_apps(self):
            self.assertEqual(Story8Config.name, 'story8')
            self.assertEqual(apps.get_app_config('story8').name, 'story8')