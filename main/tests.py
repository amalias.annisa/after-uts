from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest

class MainTestCase(TestCase):
    def test_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_landing_page_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')