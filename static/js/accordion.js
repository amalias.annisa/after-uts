AOS.init();

    $('#activity-btn').click(function () {
        $('#activity-body').toggleClass('hidden');
    })
    $('#experience-btn').click(function () {
        $('#experience-body').toggleClass('hidden');
    })
    $('#achievement-btn').click(function () {
        $('#achievement-body').toggleClass('hidden');
    })
    $('#contact-btn').click(function () {
        $('#contact-body').toggleClass('hidden');
    })

    $('.accordion-button-up').click(function () {
        var thisAccordion = $(this).parent().parent().parent();
        thisAccordion.insertBefore(thisAccordion.prev()).hide().show('slow');;
    })

    $('.accordion-button-down').click(function () {
        var thisAccordion = $(this).parent().parent().parent();
        thisAccordion.insertAfter(thisAccordion.next()).hide().show('slow');;
    })